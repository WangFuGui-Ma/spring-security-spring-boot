package com.wangfugui.apprentice.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author masiyi
 * @Date 2023/10/12 11:03
 * @PackageName:com.wangfugui.apprentice.common.enums
 * @ClassName: RoleEnum
 * @Description: TODO
 * @Version 1.0
 */
@Getter
@AllArgsConstructor
public enum RoleEnum {
    ADMIN(1, "管理员"),
    NORMAL(2,"普通用户");

    private Integer id;
    private String desc;
}
